import request from '../utils/request.js'

/**设置促销活动 */
export const postPromotions = (shopId, params) => {
  return request.post(`/v1/shops/${shopId}/promotions`, params)
}

