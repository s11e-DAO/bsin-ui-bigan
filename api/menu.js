import request from '../utils/request.js'

/**新建商品分类 */
export const postMenuCat = (shopId, params) => {
  return request.post(`/v1/shops/${shopId}/menuCats`, params)
}

/**商品类目相关接口 */
export const getMenuCats = (shopId) => {
  return request.get(`/v1/shops/${shopId}/menuCats`)
}

/**编辑商品分类 */
export const putMenuCat = (shopId, catId, params) => {
  return request.put(`/v1/menuCats/${catId}`, params)
}

/**删除商品分类 */
export const deleteMenuCat = (shopId, catId) => {
  return request.delete(`/v1/menuCats/${catId}`)
}

/**商品分类排序 */
export const putMenuCatSort = (params) => {
  return request.post(`/v1/menuCats/sort`, params)
}

/**商品相关接口 */
export const getGoods = (shopId, params) => {
  return request.get(`/v1/shops/${shopId}/menuGoods`, { params })
}

export const postGoods = (params, shopId) => {
  return request.post(`/v1/shops/${shopId}/goods`, params)
}

export const deleteGoods = (id) => {
  return request.delete(`/v1/goods/${id}`)
}

export const putGoods = (params) => {
  return request.put(`/v1/goods/${params.id}`, params)
}

export const putGoodsSort = (params) => {
  return request.post(`/v1/goods/sort`, params)
}