import request from '../utils/requestCommon.js'

export const getPhoneCode = (params) => {
  return request.get(`/v1/sms/getCode`, { params })
}
