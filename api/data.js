import request from '../utils/request.js'

export const getHomeData = (shopId) => {
  return request.get(`/v1/shops/${shopId}/home`)
}
