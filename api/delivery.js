import request from '../utils/request.js'

/**设置外送开关 */
export const postDeliveryActive = (shopId, params) => {
  return request.post(`/v1/shops/${shopId}/delivery/active`, params)
}

export const postDelivery = (shopId, params) => {
  return request.post(`/v1/shops/${shopId}/delivery`, params)
}

export const getDelivery = (shopId, params) => {
  return request.get(`/v1/shops/${shopId}/delivery`, params)
}
