import * as user from './user.js'
import * as menu from './menu.js'
import * as order from './order.js'
import * as table from './table.js'
import * as member from './member.js'
import * as delivery from './delivery.js'
import * as data from './data.js'
import * as common from './common.js'

const service = {
  ...user,
  ...menu,
  ...order,
  ...table,
  ...member,
  ...delivery,
  ...data,
  ...common
}
export default service
