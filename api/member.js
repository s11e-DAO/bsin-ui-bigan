import request from '../utils/request.js'
/**会员列表 */
export const getMembers = (shopId, params) => {
  return request.get(`/v1/shops/${shopId}/members`, { params })
}

/**首页数据 */
export const getMembersHome = (shopId, params) => {
  return request.get(`/v1/shops/${shopId}/members/home`, { params })
}

/**会员充值消费记录 */
export const getMembersFinanceLog = (shopId, params) => {
  return request.get(`/v1/shops/${shopId}/members/financeLog`, { params })
}

/**设置促销活动 */
export const postPromotions = (shopId, params) => {
  return request.post(`/v1/shops/${shopId}/promotions`, params)
}

/**查看促销活动 */
export const getPromotions = (shopId, params) => {
  return request.get(`/v1/shops/${shopId}/promotions`, { params })
}

/**关闭活动 */
export const closePromotions = (shopId, params) => {
  return request.post(`/v1/shops/${shopId}/promotions/close`, { params })
}

