import request from '../utils/request.js'

export const getOrders = (params, shopId) => {
  return request.get(`/v1/shops/${shopId}/orders`, {
    params
  })
}

export const getDeliveryOrders = (params, shopId) => {
  return request.get(`/v1/shops/${shopId}/deliveryOrders`, {
    params
  })
}

/**外送订单接单 */
export const takeDeliveryOrder = (params, shopId) => {
  return request.post(`/v1/shops/${shopId}/deliveryOrder/take`, params)
}
/**外送订单配送 */
export const shipDeliveryOrder = (params, shopId) => {
  return request.post(`/v1/shops/${shopId}/deliveryOrder/ship`, params)
}

/**处理店内订单 */
export const handleOrder = (params, shopId) => {
  return request.post(`/v1/shops/${shopId}/orders/handle`, params)
}
