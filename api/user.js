import request from '../utils/request.js'
import bsinRequest from '../utils/bsinRequest.js'

// export const login = (params) => {
//   return request.post('/v1/login', params)
// }

export const login = (params) => {
  return bsinRequest.post('', 
  {
  	serviceName: 'MerchantService',
  	methodName: 'login',
  	version: "1.0",
  	bizParams: {
  		...params
  	}})
}

// 获取所有租户
export const getTenantList = (params) => {
  return bsinRequest.post('', 
  {
    serviceName: 'TenantService',
    methodName: 'getAllTenantList',
    bizParams: {
      ...params,
    },
  });
}

export const resetPwd = (params) => {
  return request.post('/v1/auth/resetPwd', params)
}

export const forgetPwd = (params) => {
  return request.post('/v1/auth/forgetPwd', params)
}

export const register = (params) => {
  return request.post('/v1/register', params)
}

export const getUserInfo = params => {
  return request.get('/v1/orders', {
    params
  })
}

export const getMainShop = () => {
  return request.get('/v1/mainShop')
}

export const getShop = (id) => {
  return request.get(`/v1/shop/${id}`)
}

export const getShops = (id) => {
  return request.get(`/v1/shops`)
}

export const putShop = (id, params) => {
  return request.put(`/v1/shops/${id}`, params)
}
