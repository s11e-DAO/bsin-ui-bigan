import request from '../utils/request.js'

export const getTableGroups = (shopId, params) => {
  return request.get(`/v1/shops/${shopId}/tableGroups`, {
    params
  })
}

export const postTableGroups = (shopId, params) => {
  return request.post(`/v1/shops/${shopId}/tableGroups`, params)
}

export const putTableGroups = (tableGroupsId, params) => {
  return request.put(`/v1/tableGroups/${tableGroupsId}`, params)
}

export const deleteTableGroups = (tableGroupsId) => {
  return request.delete(`/v1/tableGroups/${tableGroupsId}`)
}

export const postTables = (shopId, params) => {
  return request.post(`/v1/shops/${shopId}/tables`, params)
}

export const deleteTables = (id) => {
  return request.delete(`/v1/tables/${id}`)
}

export const getTables = (id) => {
  return request.get(`/v1/tables/${id}`)
}

export const putTables = (id, params) => {
  return request.put(`/v1/tables/${id}`, params)
}