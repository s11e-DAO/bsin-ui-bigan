const getters = {
  connectBleIds: state => state.printer.bleList.map(v => v.deviceId) || [],
  bleList: state => state.printer.bleList || [],
  settings: state => state.app.settings || {},
  bleStatus: state => state.printer.openStatus || 0,
  activeShop: state => {
    let index = state.user.shopList.findIndex(v => v.id == state.user.activeShopId)
    return state.user.shopList[index] || {}
  },
  deliveryBusinessStatus: getters => getters.activeShop && getters.activeShop.deliveryBusinessStatus || 0,
  isDelivery: getters => getters.activeShop && getters.activeShop.isDelivery,
  shopId: state => state.user.activeShopId,
  menus: state => state.menus.menus || [],
  homeData: state => state.data.homeData || {},
  messageNum: state => state.data.messageNum || 0,
  goodsList: state => {
    let goodsList = []
    state.menus.menus.forEach(v => {
      goodsList = [...goodsList, ...v.goods]
    })
    return goodsList
  },
  recommendList: (state, getters) => getters.goodsList.filter(v => v.isRecommend == 1),
  global: state => state.app.global
}
export default getters
