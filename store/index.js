import Vue from 'vue'
import Vuex from 'vuex'
import app from './modules/app.js'
import user from './modules/user.js'
import socket from './modules/socket.js'
import printer from './modules/printer.js'
import order from './modules/order.js'
import menus from './modules/menus.js'
import goods from './modules/goods.js'
import table from './modules/table.js'
import member from './modules/member.js'
import delivery from './modules/delivery.js'
import data from './modules/data.js'

import getters from './getters.js'
Vue.use(Vuex)

const store = new Vuex.Store({
  modules: {
    app,
    user,
    socket,
    printer,
    order,
    menus,
    goods,
    table,
    member,
    delivery,
    data
  },
  getters
})

export default store
