import {
  SET_CAT_EDIT_TYPE,
  SET_CAT_LIST,
  ADD_ITEM_TO_MENUS,
  DELETE_CAT,
  UPDATE_CAT,
  SET_MENUS,
  ADD_NEW_GOODS,
  UPDATE_GOODS_LIST,
  UPDATE_GOODS_ITEM
} from './mutation_types.js'
import {
  EDIT_TYPE
} from '../../config/consts.js'
import * as service from '../../api/menu.js'

import { moveListItem } from '../../utils/menu'
const menus = {
  state: {
    catList: [],
    goodList: [],
    menus: [],
    activeIndex: 0
  },
  mutations: {
    [SET_CAT_EDIT_TYPE]: (state, { index, editType }) => {
      state.menus[index].editType = editType
    },
    [SET_CAT_LIST]: (state, list) => {
      state.catList = list
    },
    // 添加新分类
    [ADD_ITEM_TO_MENUS]: (state, item) => {
      item.goods = []
      state.menus.push(item)
    },
    // 删除分类
    [DELETE_CAT]: (state, catId) => {
      let index = state.menus.findIndex(v => v.id == catId)
      state.menus.splice(index, 1)
    },
    // 更新分类
    [UPDATE_CAT]: (state, { catId, params }) => {
      let index = state.menus.findIndex(v => v.id == catId)
      for (const key in params) {
        if (state.menus[index].hasOwnProperty(key)) {
          state.menus[index][key] = params[key]
        }
      }
    },
    // 设置菜单
    [SET_MENUS]: (state, list) => {
      state.menus = list
    },
    // 添加新的商品
    [ADD_NEW_GOODS]: (state, data) => {
      let index = state.menus.findIndex(v => v.id == data.catId)
      state.menus[index].goods.push(data)
    },
    // 更新商品列表
    [UPDATE_GOODS_LIST]: (state, data) => {
      let { catId, list } = data
      let index = state.menus.findIndex(v => v.id == catId)
      if (index > -1) state.menus[index].goods = list
    },
    // 
    [UPDATE_GOODS_ITEM]: (state, data) => {
      let cat = state.menus.find(v => v.id == data.catId)
      let goodsItem = cat.goods.find(v => v.id == data.id)
      for (const key in data) {
        goodsItem[key] = data[key]
      }
    }
  },
  actions: {
    async getCatList({ commit, getters }, sid) {
      let shopId = sid || getters.shopId
      let { data } = await service.getMenuCats(shopId)
      // commit(SET_CAT_LIST, data.rows)
      return data
    },
    async postMenuCat({ commit, getters }, { sid, params }) {
      let shopId = sid || getters.shopId
      let { data } = await service.postMenuCat(shopId, params)
      commit(ADD_ITEM_TO_MENUS, data)
      return data
    },
    async putMenuCat({ commit, getters }, { sid, catId, params }) {
      let shopId = sid || getters.shopId
      let res = await service.putMenuCat(shopId, catId, params)
      commit(UPDATE_CAT, { catId, params })
      return res
    },
    async deleteMenuCat({ commit, getters }, { sid, catId }) {
      let shopId = sid || getters.shopId
      let res = await service.deleteMenuCat(shopId, catId)
      commit(DELETE_CAT, catId)
      return res
    },
    async getMenus({ commit, getters }) {
      let shopId = getters.shopId
      let { data } = await service.getGoods(shopId)
      commit(SET_MENUS, data)
      return data
    },
    async putMenuCatSort({ commit }, params) {
      let res = await service.putMenuCatSort(params)
      return res
    },
    async putGoodsSort({ commit }, params) {
      let res = await service.putGoodsSort(params)
    }
  }
}
export default menus
