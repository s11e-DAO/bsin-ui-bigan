import { SET_ORDER_INFO, SET_DELIVERY_ORDER_LIST } from './mutation_types.js'
import * as service from '../../api/order'
const order = {
  state: {
    total: '0.00', // 总金额
    count: 0, // 总单数
    list: [],
    deliveryOrderList: []
  },
  mutations: {
    [SET_ORDER_INFO]: (state, data) => {
      state.total = data.total || '0.00'
      state.count = data.count || 0
      state.list = data.rows || []
    },
    [SET_DELIVERY_ORDER_LIST]: (state, data) => {
      state.deliveryOrderList = data.rows || []
    }
  },
  actions: {
    async getOrders({ getters, commit }, params) {
      let shopId = getters.shopId
      let res = await service.getOrders(params, shopId)
      if (!res.code) {
        commit(SET_ORDER_INFO, res.data)
      }
      return res.data
    },
    async getDeliveryOrders({ getters, commit }, params) {
      let shopId = getters.shopId
      let res = await service.getDeliveryOrders(params, shopId)
      if (!res.code) {
        commit(SET_DELIVERY_ORDER_LIST, res.data)
      }
      return res.data
    },
    async takeDeliveryOrder({ getters, commit }, params) {
      let shopId = getters.shopId
      let res = await service.takeDeliveryOrder(params, shopId)
      return res
    },
    async shipDeliveryOrder({ getters, commit }, params) {
      let shopId = getters.shopId
      let res = await service.shipDeliveryOrder(params, shopId)
      return res
    },
    async handleOrder({ getters, commit }, params) {
      let shopId = getters.shopId
      let res = await service.handleOrder(params, shopId)
      return res
    },
  }
}
export default order
