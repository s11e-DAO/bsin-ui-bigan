// import io from 'socket.io-client'
import io from '../../utils/uniapp.socket.io'
import dayjs, { unix } from 'dayjs'
import { SET_SOCKET_STATUS, ADD_MESSAGE_NUM, ADD_PENDING_DELIVERY_ORDER, ADD_HOME_DATA_FIELD, SET_SOCKET_BREAK_STATUS } from './mutation_types.js'
import { SOCKET_URL } from '../../config/index.js'
import { PUSH_PAYLOAD } from '../../config/consts.js'
import Vue from 'vue'
import * as socketTypes from './socket_types.js'
import { orderData, deliveryOrderData } from '../../utils/printerData'
import play from '../../utils/play'
// import Printer from '../../utils/ble/index.js'
// import Printer from '../../plugins/printer'
// import Printer from '../../utils/printer'
// let printer = new Printer(false)
let _vm = new Vue()
let socketTask = null;
const socket = {
  state: {
    status: false, // socket链接状态
    breakStatus: false
  },
  mutations: {
    [SET_SOCKET_STATUS]: (state, status) => {
      state.status = status;
    },
    [SET_SOCKET_BREAK_STATUS]: (state, breakStatus) => {
      state.breakStatus = breakStatus;
    },
  },
  actions: {
    // 初始化全局socket
    initSocket({
      commit,
      getters,
      dispatch
    }, data) {

      console.log('initSocket')
      function init() {
        socketTask = io(SOCKET_URL, {
          query: {
            shopId: getters.shopId
          }
        })
        // uni.showToast({
        //   icon: "none",
        //   title: "initSocket"
        // })
      }
      init()
      // socketTask.open
      socketTask.on('connect', (data) => {
        // uni.showToast({
        //   icon: "none",
        //   title: "socketTask连接成功"
        // })
        commit(SET_SOCKET_STATUS, true)
        commit(SET_SOCKET_BREAK_STATUS, false)
        console.log('socketTask连接成功', data)
      })
      socketTask.on('disconnect', (data) => {
        // uni.showToast({
        //   icon: "none",
        //   title: "socketTask断开"
        // })
        commit(SET_SOCKET_STATUS, false)
        console.log('socketTask断开', data)
      })
      socketTask.on('res', (data) => {
        // uni.showToast({
        //   icon: "none",
        //   title: "socketTask res" + data
        // })
        console.log('socketTask res', data)
      })
      // 接收到实时订单
      socketTask.on(socketTypes.SHOP_ORDER_CHANGE, (res) => {
        let { data } = res
        let { autoPrint } = getters.settings
        console.log(socketTypes.SHOP_ORDER_CHANGE, res)
        let messaageType = ''
        let orderTypeStr = ''
        commit(ADD_HOME_DATA_FIELD, { key: "turnover", value: data.order.total })
        commit(ADD_HOME_DATA_FIELD, { key: "dealCount", value: 1 })

        let date = dayjs(data.order.createdAt).format('HH:mm:ss')
        let option = { sound: "none" }
        if (data.order.hasOwnProperty('deliveryFee')) {
          commit(ADD_PENDING_DELIVERY_ORDER)
          messaageType = PUSH_PAYLOAD.SOCKET_DELIVERY_ORDER
          orderTypeStr = `外送`
          // #ifdef APP-PLUS
          plus.push.createMessage(`${date} 您有一个新的${orderTypeStr}订单，请注意处理`, JSON.stringify({ type: messaageType }), option)
          // #endif
          play.play('/static/music/new_delivery_order.mp3')
        } else {
          messaageType = PUSH_PAYLOAD.SOCKET_ORDER
          orderTypeStr = `店内`
          // #ifdef APP-PLUS
          plus.push.createMessage(`${date} 您有一个新的${orderTypeStr}订单，请注意处理`, JSON.stringify({ type: messaageType }), option)
          // #endif
          play.play('/static/music/new_order.mp3')
        }

        if (autoPrint) dispatch('socketPrint', data.order)
      })
    },
    // 获取实时订单
    getRealTimeOrder() {

    },
    // 关闭socke链接
    closeSocket() {
      console.log('closeSocket')
      socketTask.disconnect()
    },

    // 打印小票
    socketPrint({ getters }, data) {
      let buff = data.hasOwnProperty('deliveryFee') ? deliveryOrderData(data) : orderData(data)
      let bleList = getters.bleList
      let settings = getters.settings
      _vm.$printerUtil.printToAll(buff, () => { }, bleList, settings)
    }
  }
}
export default socket
