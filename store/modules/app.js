import {
  SET_SETTINGS,
  SYNC_SETTINGS,
  SET_PRINTER_SETTINGS,
  SET_GLOBAL_DATA
} from './mutation_types.js'
import { STORAGE_KEY } from '../../config/consts.js'
const app = {
  state: {
    settings: {
      number: 1, // 打印分数
      showShopName: true,
      autoPrint: true, // 是否自动打印
      lineSpace: 3, // 打印行距
      printer: {
        number: 1, // 打印分数
        showShopName: true, // 是否显示店名
        autoPrint: true, // 是否自动打印
        lineSpace: 3,// 打印行距
      }
    },
    global: {
      ORDER_PAGE_INIT_DATA: null,
      ORDER_PAGE_HAS_SHOW: false,
    }
  },

  mutations: {
    // 设置
    [SET_SETTINGS]: (state, data) => {
      for (const key in data) {
        state.settings[key] = data[key]
      }
      uni.setStorageSync(STORAGE_KEY.APP_SETTINGS, state.settings)
    },
    // 打印机设置
    [SET_PRINTER_SETTINGS]: (state, data) => {
      for (const key in data) {
        state.settings.printer[key] = data[key]
      }
      uni.setStorageSync(STORAGE_KEY.APP_SETTINGS, state.settings)
    },
    // 同步设置
    [SYNC_SETTINGS]: (state) => {
      let settings = uni.getStorageSync(STORAGE_KEY.APP_SETTINGS) || null
      if (settings) {
        for (let key in state.settings) {
          if (settings.hasOwnProperty(key)) state.settings[key] = settings[key]
        }
      } else {
        uni.setStorageSync(STORAGE_KEY.APP_SETTINGS, state.settings)
      }
    },
    //
    [SET_GLOBAL_DATA]: (state, data) => {
      for (let key in data) {
        state.global[key] = data[key]
      }
    }
  }
}

export default app
