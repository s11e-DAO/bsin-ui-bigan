import { SET_MEMBER_LIST, SET_PROMOTION_LIST, SET_MEMBER_HOME_DATA, SET_FINANCE_LOGS } from './mutation_types.js'
import service from '../../api/index.js'
const menus = {
  state: {
    memberList: [],
    promotionList: [],
    recharge: {
      // "createdAt": "2020-06-12 15:18:01",
      // "updatedAt": "2020-06-12 15:18:01",
      // "id": 6,
      // "shopId": 2,
      // "type": 1,
      // "config": [{ "amount": 200, "giveAmount": 20 }, { "amount": 300, "giveAmount": 30 }],
      // "status": 1
    },
    home: {},
    financeLogs: []
  },
  mutations: {
    [SET_MEMBER_LIST]: (state, list) => {
      state.memberList = list
    },
    [SET_PROMOTION_LIST]: (state, list) => {
      state.promotionList = list
    },
    [SET_MEMBER_HOME_DATA]: (state, data) => {
      state.home = data
    },
    [SET_FINANCE_LOGS]: (state, list) => {
      state.financeLogs = list
    },
  },
  actions: {
    async getMembers({ commit, getters }, params) {
      let shopId = getters.activeShop.id
      let { data } = await service.getMembers(shopId, params)
      commit(SET_MEMBER_LIST, data.rows)
      return data
    },
    async getMembersHome({ commit, getters }, params) {
      let shopId = getters.activeShop.id
      let { data } = await service.getMembersHome(shopId, params)
      commit(SET_MEMBER_HOME_DATA, data)
      return data
    },
    async getMembersFinanceLog({ commit, getters }, params) {
      let shopId = getters.activeShop.id
      let { data } = await service.getMembersFinanceLog(shopId, params)
      commit(SET_FINANCE_LOGS, data.rows)
      return data
    },
    async getPromotions({ commit, getters }, params) {
      let shopId = getters.activeShop.id
      let { data } = await service.getPromotions(shopId)
      commit(SET_PROMOTION_LIST, data)
      return data
    },
    async postPromotions({ commit, getters }, params) {
      let shopId = getters.activeShop.id
      let res = await service.postPromotions(shopId, params)
      return res
    },
    async closePromotions({ commit, getters }, type) {
      let shopId = getters.activeShop.id
      let res = await service.closePromotions(shopId, type)
      return res
    }
  }
}
export default menus
