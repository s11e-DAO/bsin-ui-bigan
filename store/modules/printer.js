import {
  SET_BLUETOOTH_INFO,
  UPDATE_BLUETOOTH_INFO,
  SET_BLUETOOTH_STATUS,
  ADD_MEW_BLUETOOTH,
  REMOVE_BLUETOOTH,
  ADD_BUFF_TO_PRINTER,
  DELETE_BUFF_FROM_PRINTER,
  SYNC_BLUETOOTH_INFO
} from './mutation_types.js'
let sysinfo = uni.getSystemInfoSync();
import {
  STORAGE_KEY
} from '../../config/consts.js'
// 数据结构
let bleInfo = {
  platform: sysinfo.platform,
  deviceId: "",
  name: "",
  connected: true, // 连接状态
  advertisServiceUUIDs: [],
  services: [],
  readCharaterId: "",
  readServiceId: "",
  writeCharaterId: "",
  writeServiceId: "",
  notifyCharaterId: "",
  notifyServiceId: "",
  indicateCharaterId: "",
  indicateServiceId: "",
  buffList: [], // 待处理订单
  running: false
}
const printer = {
  state: {
    // 已经连接的设备
    bleList: [
      // {buffList:[],running:false}
    ],
    status: false, // 连接状态 true已连接，false未连接
    openStatus: false, // 蓝牙模块是否初始化
    breakType: "", // 断开类型
  },
  mutations: {
    [SYNC_BLUETOOTH_INFO]: (state) => {
      state.bleList = uni.getStorageSync(STORAGE_KEY.BLUETOOTH_INFO) || []
    },
    // 只修改vuex中的内容
    [SET_BLUETOOTH_INFO]: (state, data) => {
      let deviceId = data.deviceId
      let index = state.bleList.findIndex(item => item.deviceId == deviceId)
      let localData = uni.getStorageSync(STORAGE_KEY.BLUETOOTH_INFO) || []
      let isNewBle = !localData.some(item => item.deviceId == data.deviceId) // 判断是否为新的蓝牙设备
      // 当不存在时新建，存在时更新
      if (index == -1) {
        let obj1 = JSON.parse(JSON.stringify(bleInfo))
        for (let key in data) {
          obj1[key] = data[key]
        }
        state.bleList.push(obj1)
      } else {
        let obj2 = state.bleList[index]
        for (let key in data) {
          obj2[key] = data[key]
        }
      }
      if (isNewBle) uni.setStorageSync(STORAGE_KEY.BLUETOOTH_INFO, state.bleList) // 保存连接的蓝牙打印机信息
    },
    [UPDATE_BLUETOOTH_INFO]: (state, data) => {
      let bleItem = state.bleList.find(v => v.deviceId == data.deviceId)
      for (let key in data) {
        bleItem[key] = data[key]
      }
    },
    // 设置当前蓝牙初始状态
    [SET_BLUETOOTH_STATUS]: (state, status) => {
      state.openStatus = status
    },
    // 添加新设备
    [ADD_MEW_BLUETOOTH]: (state, data) => {
      let deviceId = data.deviceId
      let index = state.bleList.findIndex(item => item.deviceId == deviceId)
      // 当不存在时新建
      if (index == -1) {
        let obj1 = JSON.parse(JSON.stringify(bleInfo))
        for (let key in obj1) {
          obj1[key] = data[key]
        }
        state.bleList.push(obj1)
        uni.setStorageSync(STORAGE_KEY.BLUETOOTH_INFO, state.bleList) // 保存连接的蓝牙打印机信息
      } else {
        let obj2 = state.bleList[index]
        for (let key in obj2[index]) {
          obj2[index][key] = data[key]
        }
      }
    },
    // 移除设备
    [REMOVE_BLUETOOTH]: (state, deviceId) => {
      let index = state.bleList.findIndex(item => item.deviceId == deviceId)
      if (index > -1) state.bleList.splice(index, 1)
      uni.setStorageSync(STORAGE_KEY.BLUETOOTH_INFO, state.bleList) // 保存连接的蓝牙打印机信息
    },
    // 向打印机添加需打印数据
    [ADD_BUFF_TO_PRINTER]: (state, data) => {
      let { buff, deviceId } = data
      let printer = state.bleList.find(v => v.deviceId == deviceId)
      if (!printer.buffList) printer.buffList = []
      printer.buffList.push(buff)
      printer.running = true
    },
    // 删除数据
    [DELETE_BUFF_FROM_PRINTER]: (state, deviceId) => {
      let printer = state.bleList.find(v => v.deviceId == deviceId)
      if (printer.buffList) printer.buffList.shift()
      printer.running = false
    }
  }
}

export default printer
