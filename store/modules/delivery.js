import { SET_DELIVERY_DATA } from './mutation_types.js'
import service from '../../api/index.js'
const delivery = {
  state: {
    data: {
      phone: null,
      address: null,
      startTime: null,
      endTime: null,
      range: null,
      startPrice: null,
      boxFee: null,
      latitude: null,
      longitude: null,
      // deliveryFeeConfig: {
      //   type: "",
      //   price: "",
      //   config: {
      //     min: "",
      //     max: "",
      //     price: "",
      //   },
      // },
      deliveryFee: null,
      deliveryFeeConfig: null
    }
  },
  mutations: {
    [SET_DELIVERY_DATA]: (state, data) => {
      for (let key in data) {
        state.data[key] = JSON.parse(JSON.stringify(data[key]))
      }
    }
  },
  actions: {
    async postDelivery({ getters, commit }, params) {
      let shopId = getters.activeShop.id
      let res = await service.postDelivery(shopId, params)
      commit(SET_DELIVERY_DATA, res.data)
      return res
    },
    async getDelivery({ getters, commit }) {
      let shopId = getters.activeShop.id
      let res = await service.getDelivery(shopId)
      commit(SET_DELIVERY_DATA, res.data)
      return res
    },
    async postDeliveryActive({ getters, commit }, params) {
      let shopId = getters.activeShop.id
      let res = await service.postDeliveryActive(shopId, params)
      return res
    }
  }
}
export default delivery
