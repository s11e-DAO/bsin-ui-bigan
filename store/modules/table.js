import {
  SET_TABLE_GROUP_LIST, SET_TABLE_LIST,
  ADD_NEW_TABLE, DELETE_TABLE_GROUP,
  UPDATE_TABLE_GROUP, ADD_NEW_TABLE_GROUP,
  SET_TABLE_INFO, DELETE_TABLE, UPDATE_TABLE_INFO
} from './mutation_types.js'
import service from '../../api/index'
const temp = {
  state: {
    groupList: [],
    tableList: [],
    tableInfo: {}
  },
  mutations: {
    [SET_TABLE_GROUP_LIST]: (state, list) => {
      state.groupList = list
    },
    [SET_TABLE_LIST]: (state, list) => {
      state.tableList = list
    },
    // 新建桌台
    [ADD_NEW_TABLE]: (state, data) => {
      let group = state.groupList.find(v => v.id == data.groupId)
      if (group) {
        if (!group.tables) group.tables = []
        group.tables.push(data)
      }
    },
    // 删除分组
    [DELETE_TABLE_GROUP]: (state, id) => {
      let index = state.groupList.findIndex(v => v.id == id)
      if (index > -1) state.groupList.splice(index, 1)
    },
    // 更新分组信息
    [UPDATE_TABLE_GROUP]: (state, data) => {
      let group = state.groupList.find(v => v.id == data.id)
      if (group) {
        for (const key in data) {
          group[key] = data[key]
        }
      }
    },
    // 添加新的分组到list中
    [ADD_NEW_TABLE_GROUP]: (state, data) => {
      if (!data.tables) data.tables = []
      state.groupList.push(data)
    },
    // 设置桌台详情
    [SET_TABLE_INFO]: (state, tableInfo) => {
      state.tableInfo = tableInfo
    },
    // 删除桌台
    [DELETE_TABLE]: (state, data) => {
      let group = state.groupList.find(v => v.id == data.groupId)
      let index = group.tables.findIndex(v => v.id == data.id)
      group.tables.splice(index, 1)
    },
    // 更新桌台信息
    [UPDATE_TABLE_INFO]: (state, data) => {
      let group = state.groupList.find(v => v.id == data.groupId)
      let table = group.tables.find(v => v.id == data.id)
      if (table) {
        for (const key in data) {
          table[key] = data[key]
        }
      }
    },
  },
  actions: {
    /**分区 */
    async getTableGroups({ commit, getters }, params) {
      let { data } = await service.getTableGroups(getters.shopId, params)
      commit(SET_TABLE_GROUP_LIST, data)
      return data
    },
    async postTableGroups({ commit, getters }, params) {
      let res = await service.postTableGroups(getters.shopId, params)
      if (!res.code) {
        commit(ADD_NEW_TABLE_GROUP, res.data)
      }
      return res
    },
    async putTableGroups({ commit }, params) {
      let res = await service.putTableGroups(params.id, params)
      if (!res.code) {
        commit(UPDATE_TABLE_GROUP, params)
      }
      return res
    },
    async deleteTableGroups({ commit }, tableGroupsId) {
      let res = await service.deleteTableGroups(tableGroupsId)
      if (!res.code) {
        commit(DELETE_TABLE_GROUP, tableGroupsId)
      }
      return res
    },
    /**桌台 */
    async postTables({ commit, getters }, params) {
      let res = await service.postTables(getters.shopId, params)
      if (!res.code) {
        commit(ADD_NEW_TABLE, res.data)
      }
      return res
    },
    async putTables({ commit, getters }, params) {
      let res = await service.putTables(params.id, params)
      if (!res.code) {
        commit(UPDATE_TABLE_INFO, params)
      }
      return res
    },
    async deleteTables({ commit, getters }, tableInfo) {
      let res = await service.deleteTables(tableInfo.id)
      if (!res.code) {
        commit(DELETE_TABLE, tableInfo)
      }
      return res
    },
    async getTables({ commit, getters }, id) {
      let { data } = await service.getTables(id)
      return data
    },
  }
}
export default temp
