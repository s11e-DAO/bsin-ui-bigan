import { SET_HOME_DATA, ADD_MESSAGE_NUM, ADD_PENDING_DELIVERY_ORDER, ADD_HOME_DATA_FIELD } from './mutation_types.js'
import { STORAGE_KEY } from '../../config/consts.js'
import service from '../../api/index'
const app = {
  state: {
    homeData: {
      orderTurnover: 0, // 店内订单营业额	
      dOrderTurnover: 0, // 外送订单营业额	
      turnover: 0, // 总营业额	
      orderDealCount: 0, // 店内订单成交数	
      dOrderDealCount: 0, // 外送订单成交数	
      dealCount: 0, // 成交订单总数	
      pendingOrder: 0, // 店内待处理订单数	
      pendingDeliveryOrder: 0, // 待配送订单数	
      newMemberNum: 0, // 新增会员人数	
      memberCost: 0, // 会员消费额	
      memberCharge: 0, // 会员充值	
    },
    messageList: [],
    messageNum: 0
  },
  mutations: {
    [SET_HOME_DATA]: (state, data) => {
      state.homeData = data
    },
    [ADD_MESSAGE_NUM]: (state, num = 1) => {
      state.messageNum = state.messageNum + num
    },
    [ADD_PENDING_DELIVERY_ORDER]: (state, num = 1) => {
      state.homeData.pendingDeliveryOrder = state.homeData.pendingDeliveryOrder + num
    },
    [ADD_HOME_DATA_FIELD]: (state, data) => {
      let { key, value } = data
      state.homeData[key] = state.homeData[key] + (value || 1)
    },
  },
  actions: {
    async getHomeData({ getters, commit }) {
      let shopId = getters.shopId
      let { data } = await service.getHomeData(shopId)
      commit(SET_HOME_DATA, data)
      return data
    }
  }
}

export default app
