## 项目配置

### 请求地址

- /config/index.js

```js
//基础请求地址
export const BASE_URL = "http://123.249.75.204:7002";

//通用请求地址
export const COMMON_BASE_URL = "";

// 图片上传地址
export const UPLOAD_PATH = "";

// oss 图片地址
export const FILE_PATH = "";

// socket地址
export const SOCKET_URL = "";

export const HEADER_ANTHORIZATION = "Authorization";

// 登录有效时长 ms 7天expire
export const EXPIRE_TIME = 1000 * 60 * 60 * 24 * 15;

// 二维码地址信息 http://www.maiziit.cn
export const qrcodeBaseUrl = "";
```

### 高德地图
