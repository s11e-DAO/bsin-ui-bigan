
function getCode128(val) {
	// 开始位为固定格式 str.charCodeAt()
	let code128A = [0x7B, 0x41]
	let code128B = [0x7B, 0x42]
	let code128C = [0x7B, 0x43]
	let ret = [];
	let n67 = 0;
	for (let i = 0; i < val.length; i += 2) {
		let tmp = val.substr(i, 2);
		if (tmp == '00') {
			if (i == 0) {
				ret.push(123);
				ret.push(66);
				n67 = 66;
			} else if (n67 == 67) {
				ret.push(123);
				ret.push(66);
				n67 = 66;
			}

			ret.push(48);
			ret.push(48);
		} else {
			if (i == 0) {
				ret.push(123);
				ret.push(67);
				n67 = 67;
			} else if (n67 == 66) {
				ret.push(123);
				ret.push(67);
				n67 = 67;
			}
			ret.push(parseInt(tmp));
		}
	}
	ret.unshift(ret.length)

	return ret;
}
export default getCode128
// module.exports = getCode128;
