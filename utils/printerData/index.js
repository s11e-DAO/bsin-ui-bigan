/**
 * 普通店内订单数据
 * @param {*} config 
 */
import orderData from './orderData'
/**
 * 外送订单数据
 * @param {*} config 
 */
import deliveryOrderData from './deliveryOrderData'
/**
 * 普通订单测试数据
 * @param {*} config 
 */
import orderTestData from './orderTestData'

/**
 * 二维码
 */

import qrCodeData from './qrCodeData'

import printEscData from './printEscData'

export { orderData, printEscData, deliveryOrderData, orderTestData, qrCodeData }