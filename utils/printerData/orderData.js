/**
 * 打印数据处理
 */
import escPrinter from '../ble/esc.js'
import store from '../../store/index'
import Utils from './utils'

const orderData = (config) => {
  // let {
  //   shopName,
  //   tableNo,
  //   tableInfo,
  //   buyerRemark,
  //   createdAt,
  //   total,
  //   orderGoods,
  //   orderNo
  // } = config
  let settings = store && store.getters && store.getters.settings || {}
  const utils = new Utils(settings.lineSpace)
  let shop = store.getters.activeShop
  let command = escPrinter.createNew()
  let areaName = config.tableInfo && (config.tableInfo.groupName + config.tableInfo.tableNo) || ''
  command.init()
  // 判断是否打印店铺名
  if (settings.showShopName) {
    utils.printShopName(command, shop.shopName)
  }

  /**桌号 setPrintAndFeedRow*/
  utils.printAreaInfo(command, areaName)

  /**头部信息 */
  utils.printHeader(command, config.createdAt, config.orderNo)

  /**虚线 */
  utils.printDotted(command)

  /**商品信息 */
  utils.printGoods(command, config.orderGoods)

  /**虚线 */
  utils.printDotted(command)

  /**备注 */
  utils.printRemarkData(command, config.buyerRemark)

  /**虚线 */
  utils.printDotted(command)

  utils.printTotal(command, config.total)

  return command.getData()
}
export default orderData