/**
 * 打印数据处理
 */
import escPrinter from '../ble/esc.js'
import store from '../../store/index'
import Utils from './utils'

const qrCodeData = (data) => {
  let settings = store && store.getters && store.getters.settings || {}
  const utils = new Utils(settings.lineSpace)
  let command = escPrinter.createNew()
  command.init()
  utils.printCode(command, data)
  return command.getData()
}
export default qrCodeData