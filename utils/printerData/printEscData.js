/**
 * 打印数据处理
 */
import escPrinter from '../ble/esc.js'
import store from '../../store/index'
import dayjs from 'dayjs'
import conf from './conf.json'
let loopPrint = (command, list, maxLength) => {
  command.setText(`${list.splice(0, maxLength).join("")}`);
  command.setPrint()
  if (list.length > 0) loopPrint(command, list, maxLength)
}

let setGoodsData = (command, orderGoods) => {
  let titleMax = 9
  /**表头 */
  command.setAbsolutePrintPosition(0);
  command.setText("商品");
  command.setAbsolutePrintPosition(250);
  command.setText("数量");
  command.setAbsolutePrintPosition(310);
  command.setText("单价");
  command.setPrint()
  orderGoods.forEach(goods => {
    let titleList = goods.title.split("");
    let priceList = (goods.price).toString().split("");
    command.setAbsolutePrintPosition(0);
    command.setText(`${titleList.splice(0, titleMax).join("")}`);
    command.setAbsolutePrintPosition(255);
    command.setText(`${goods.num}`);
    command.setAbsolutePrintPosition(315);
    command.setText(`${priceList.splice(0, 5).join("")}`);
    command.setPrint()
    // 价格过长换行
    if (priceList.length > 0) {
      command.setAbsolutePrintPosition(315);
      command.setText(`${priceList.join("")}`);
      command.setPrint()
    }
    // 标题过长换行
    if (titleList.length > 0) {
      loopPrint(command, titleList, titleMax)
    }
    /**打印商品属性 */
    if (goods.props.length > 0 || (goods.spec && goods.spec.length > 0)) {
      let props = (goods.props || []).map(v => v.v[0])
      let specs = (goods.spec || []).map(v => v.value)
      let propSpecs = [...specs, ...props].join('/')
      command.setAbsolutePrintPosition(0);
      command.setText(`${propSpecs}`);
      command.setPrint()
    }
  })
}

let setRemarkData = (command, buyerRemark) => {
  /**备注 */
  let buyerRemarkList = buyerRemark.split("");
  command.setText(`备注：${buyerRemarkList.splice(0, 13).join("") || '无'}`);
  command.setPrint();
  if (buyerRemarkList.length > 0) {
    loopPrint(command, buyerRemarkList, 16)
  }
}
const printEscData = (data) => {
  let command = escPrinter.createNew()
  command.init()
  // 标题
  command.bold(1); //加粗
  command.setFontSize(16); //字体大小
  command.setSelectJustification(1); //居中
  command.rowSpace(100);
  command.setText("杭州总店");
  command.setPrint();
  command.rowSpace(60);

  command.bold(0); //取消加粗
  command.setFontSize(0); //正常字体
  //时间
  command.setSelectJustification(0); //居左
  command.setText("时间：2019-11-11 12:10:30");
  command.setPrint();
  //编号
  command.setSelectJustification(0); //居左
  command.setText("编号：SD10000000000000000");
  command.setPrintAndFeed(80); //打印并走纸feed个单位
  //列表
  command.rowSpace(80); //间距
  command.bold(5); //加粗
  command.setText("货号");
  command.setAbsolutePrintPosition(100);
  command.setText("颜色");
  command.setAbsolutePrintPosition(180);
  command.setText("尺码");
  command.setAbsolutePrintPosition(270);
  command.setText("单价");
  command.setAbsolutePrintPosition(380);
  command.setText("数量");
  command.setAbsolutePrintPosition(480);
  command.setText("金额");
  command.setPrint();
  command.bold(0); //加粗
  // -------1
  command.setText("86001W");
  command.setAbsolutePrintPosition(100);
  command.setText("黄色");
  command.setAbsolutePrintPosition(180);
  command.setText("均码");
  command.setAbsolutePrintPosition(270);
  command.setText("16947.92");
  command.setAbsolutePrintPosition(390);
  command.setText("1");
  command.setAbsolutePrintPosition(480);
  command.setText("19647.92");
  command.setPrint();
  // ------2
  command.setText("86001W");
  command.setAbsolutePrintPosition(100);
  command.setText("黄色");
  command.setAbsolutePrintPosition(180);
  command.setText("均码");
  command.setAbsolutePrintPosition(270);
  command.setText("16947.92");
  command.setAbsolutePrintPosition(390);
  command.setText("1");
  command.setAbsolutePrintPosition(480);
  command.setText("19647.92");
  command.setPrint();
  // -------3
  command.setText("86001W");
  command.setAbsolutePrintPosition(100);
  command.setText("黄色");
  command.setAbsolutePrintPosition(180);
  command.setText("均码");
  command.setAbsolutePrintPosition(270);
  command.setText("16947.92");
  command.setAbsolutePrintPosition(390);
  command.setText("1");
  command.setAbsolutePrintPosition(480);
  command.setText("19647.92");
  command.setPrint();

  //合计
  command.bold(5); //加粗
  command.setAbsolutePrintPosition(120);
  command.setText("总数：10");
  command.setAbsolutePrintPosition(320);
  command.setText("合计：10000");
  command.setPrint();
  command.setAbsolutePrintPosition(120);
  command.setText("实收：10000");
  command.setAbsolutePrintPosition(320);
  command.setText("找零：0");
  command.setPrint();
  // 收银员
  command.rowSpace(120); //间距
  command.setAbsolutePrintPosition(120);
  command.setText("店员：何丹");
  command.setAbsolutePrintPosition(320);
  command.setText("会员：1000000000");
  command.setPrint();

  //提示
  command.rowSpace(80); //间距
  command.bold(2); //加粗
  command.setSelectJustification(1); //居中
  command.setText("售出商品购买后7天内,可凭小票退换");
  command.setPrint();
  command.setText("(注：吊牌未拆剪,商品未洗涤)");
  command.setPrint();

  //电话
  command.setSelectJustification(0); //居左
  command.setText("客服电话:(0571)86011123");
  command.setPrint();
  command.setText("联系地址:浙江省诸暨市暨阳街道健康路1-1号");
  command.setPrint();

  command.setPrintAndFeedRow(3);

  return command.getData()
}
export default printEscData