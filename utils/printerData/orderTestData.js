/**
 * 打印数据处理
 */
import escPrinter from '../ble/esc.js'
import Utils from './utils'
import testData from './order.json'
import store from '../../store/index'

const orderTestData = () => {
  let settings = store && store.getters && store.getters.settings || {}
  const utils = new Utils(settings.lineSpace)
  
  let command = escPrinter.createNew()
  let areaName = '一楼08桌'
  let shopName = '麦子生活餐厅'
  command.init()

  utils.printShopName(command, shopName)

  /**桌号 */
  utils.printAreaInfo(command, areaName)

  /**头部信息 */
  utils.printHeader(command, testData.createdAt, testData.orderNo)

  /**虚线 */
  utils.printDotted(command)

  /**商品信息 */
  utils.printGoods(command, testData.orderGoods)

  /**虚线 */
  utils.printDotted(command)

  /**备注 */
  utils.printRemarkData(command, testData.buyerRemark)

  /**虚线 */
  utils.printDotted(command)

  utils.printTotal(command, testData.total)

  return command.getData()
}
export default orderTestData