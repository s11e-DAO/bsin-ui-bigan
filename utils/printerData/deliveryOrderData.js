/**
 * 打印数据处理
 */
import escPrinter from '../ble/esc.js'
import store from '../../store/index'
import Utils from './utils'
import {qrcodeBaseUrl} from '../../config/index'
let loopPrint = (command, list, maxLength) => {
  command.setText(`${list.splice(0, maxLength).join("")}`);
  command.setPrint()
  if (list.length > 0) loopPrint(command, list, maxLength)
}

const deliveryOrderData = (config) => {
  let settings = store && store.getters && store.getters.settings || {}
  console.log(store);

  const utils = new Utils(settings.lineSpace)
  let {
    shopName,
    tableNo,
    buyerRemark,
    createdAt,
    total,
    orderGoods,
    orderNo,
    deliveryInfo
  } = config
  let shop = store.getters.activeShop
  let command = escPrinter.createNew()
  let areaName = '外送订单'
  command.init()

  // command.rowSpace(50);
  // 判断是否打印店铺名
  if (settings.showShopName) {
    utils.printShopName(command, shop.shopName)
  }

  /**桌号 setPrintAndFeedRow*/
  utils.printAreaInfo(command, areaName)

  /**头部信息 */
  utils.printHeader(command, config.createdAt, config.orderNo)

  /**虚线 */
  utils.printDotted(command)

  /**商品信息 */
  utils.printGoods(command, orderGoods)

  /**虚线 */
  utils.printDotted(command)

  /**外送费 */
  utils.printDeliveryFee(command, config)

  /**虚线 */
  utils.printDotted(command)

  /**备注 */
  utils.printRemarkData(command, buyerRemark)

  /**虚线 */
  utils.printDotted(command)

  /**打印外送信息 */
  utils.printDeliveryInfo(command, config.deliveryInfo)

  /**虚线 */
  utils.printDotted(command)

  utils.printTotal(command, config.total)

  // let qrdata = {
  //   id: config.id,
  //   type: 'deliveryOrder'
  // }
  utils.printCode(command, `${qrcodeBaseUrl}/?sid=${store.getters.activeShop.id}&&orderId=${config.id}`)

  return command.getData()
}
export default deliveryOrderData