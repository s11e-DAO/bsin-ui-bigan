export default class Bluetooth {

  constructor(watchChange = false) {
    this.status = false // 蓝牙是否打开
    this.loading = false // 是否正在搜索
    this.list = [] //发现的设备列表
    this.searchTimer = null
    this.deviceId
    this.platform = uni.getSystemInfoSync().platform
    this.openBluetooth()
  }

  /**
   * 当蓝牙连接状态改变时
   */
  onStateChange(cb) {
    uni.onBluetoothAdapterStateChange((res) => {
      if (typeof cb == 'function') cb(res)
    })
  }
  openBluetoothAdapter() {
    return new Promise((resolve, reject) => {
      uni.openBluetoothAdapter({
        success: res => {
          this.isOpenBle = true;
          this.showToast("初始化蓝牙模块成功");
          resolve(res);
        },
        fail: err => {
          this.showToast(`初始化蓝牙模块失败` + JSON.stringify(err));
          reject(err);
        },
      });
    });
  }
  /**
   * 打开手机蓝牙
   */
  openPhoneBluetooth(cb) {
    console.log('打开手机蓝牙')
    let main, BluetoothAdapter, BAdapter
    let platform = this.platform
    let enable = false
    let res = null
    switch (platform) {
      case 'android':
        main = plus.android.runtimeMainActivity()
        BluetoothAdapter = plus.android.importClass("android.bluetooth.BluetoothAdapter")
        BAdapter = BluetoothAdapter.getDefaultAdapter()
        if (!BAdapter.isEnabled()) {
          enable = BAdapter.enable()
        }
        break;
      case 'ios':
        // main = plus.android.runtimeMainActivity()
        // BluetoothAdapter = plus.android.importClass("android.bluetooth.BluetoothAdapter")
        // BAdapter = BluetoothAdapter.getDefaultAdapter()
        // if (!BAdapter.isEnabled()) {
        //   BAdapter.enable()
        // }
        break;
      default:
        break;
    }
    return enable
  }

  /**
   * 初始化蓝牙
   */
  openBluetooth(cb) {
    uni.openBluetoothAdapter({
      success: (res) => {
        this.status = true
        if (typeof cb == 'function') cb(true, res)
      },
      fail: (e) => {
        this.status = false
        if (typeof cb == 'function') cb(false, e)
        let enable = this.openPhoneBluetooth()
        if (!enable) {
          uni.showToast({
            icon: "none",
            title: "请打开手机蓝牙"
          })
        }
      }
    })
  }

  /*
   * 搜索附近的蓝牙设备
   */
  search(config = {
    search: null,
    finish: null,
  }, finishTime = 30) {
    // 检查是否开启了蓝牙，如果未开启则先开启蓝牙
    if (this.status) {
      uni.startBluetoothDevicesDiscovery({
        success: (res) => {
          uni.onBluetoothDeviceFound(res => {
            res.devices.forEach(item => {
              if (!this.list.some(v => v.deviceId == item.deviceId)) this.list.push(item)
            })
            if (typeof config.search == 'function') config.search(this.list)
          })
          this.searchTimer = setTimeout(() => {
            if (typeof config.finish == 'function') config.finish()
            this.stopSearch()
          }, 1000 * finishTime) // 30s后关闭
        }
      })
    } else {
      this.openBluetooth(() => {
        this.search(config)
      })
    }
  }

  /*
   * 停止搜索
   */
  stopSearch(cb) {
    return new Promise((resolve, reject) => {
      uni.stopBluetoothDevicesDiscovery({
        success: (res) => {
          resolve(res)
        },
        fail: (res) => {
          reject(res)
        },
      })
    })
  }

  /**
   * 链接蓝牙设备
   * @param {*} deviceId 设备id
   */
  createBLEConnection(deviceId) {
    return new Promise((resolve, reject) => {
      uni.createBLEConnection({
        deviceId,
        success: (res) => {
          console.log("res:createBLEConnection " + JSON.stringify(res));
          resolve(res)
        },
        fail: err => {
          console.log("res:createBLEConnection " + JSON.stringify(err));
          reject(err);
        }
      })
    });
  }

  /**
   * 获取servics
   * @param {*} deviceId 
   */
  getBLEDeviceServices(deviceId) {
    return new Promise((resolve, reject) => {
      uni.getBLEDeviceServices({
        deviceId,
        success: (res) => {
          resolve(res)
          console.log('getSeviceId success', res);
        },
        fail: (e) => {
          reject(e)
          console.log('getSeviceId fail', deviceId, e);
        }
      })
    })
  }

  /**
   * 获取特征值
   * @param {*} deviceId 
   * @param {*} serviceId 
   */
  getBLEDeviceCharacteristics(deviceId, serviceId) {
    return new Promise((resolve, reject) => {
      uni.getBLEDeviceCharacteristics({
        deviceId,
        serviceId,
        success: res => {
          resolve(res)
        },
        fail: err => {
          reject(err);
        }
      })
    })
  }

  /*
   * 断开连接
   */
  disconnect({
    deviceId
  }, cb) {
    uni.closeBLEConnection({
      deviceId,
      success: res => {
        if (typeof cb == 'function') cb(res)
      },
      fail: e => {
        if (typeof cb == 'function') cb(e)
      }
    })
  }

  /*
   * 查询状态
   */
  state() { }

  /*
   * 打印小票
   * @param {number} num 打印份数
   */
  print(config, currentTime, currentPrint) {
    let {
      deviceId,
      writeServiceId,
      writeCharaterId,
      num,
      buff,
      success,
      fail,
      complete
    } = config
    let oneTimeData = 20 // 每次发送字节数量
    let printerNum = num || 1 // 打印几份
    let loopTime = parseInt(buff.length / oneTimeData) // 分包数
    let lastData = parseInt(buff.length % oneTimeData) // 最后的字节数据量
    loopTime = loopTime + 1
    currentTime = currentTime || 1 // 第几次打印
    currentPrint = currentPrint || 0 // 已经打印份数

    let buf = null
    let dataView = null
    if (currentTime < loopTime) {
      buf = new ArrayBuffer(oneTimeData);
      dataView = new DataView(buf);
      for (var i = 0; i < oneTimeData; ++i) {
        dataView.setUint8(i, buff[(currentTime - 1) * oneTimeData + i]);
      }
    } else {
      buf = new ArrayBuffer(lastData);
      dataView = new DataView(buf);
      for (var i = 0; i < lastData; ++i) {
        dataView.setUint8(i, buff[(currentTime - 1) * oneTimeData + i]);
      }
    }

    uni.writeBLECharacteristicValue({
      deviceId: deviceId,
      serviceId: writeServiceId,
      characteristicId: writeCharaterId,
      value: new Uint8Array([0, 0, 0]).buffer,
      success: (res) => {

      },
      fail: (e) => {
        if (typeof fail == 'function') fail(e)
      },
      complete: (c) => {
        currentTime++;
        if (currentTime <= loopTime) {
          this.print(config, currentTime, currentPrint);
        } else {
          currentPrint++;
          if (currentPrint == printerNum) {
            // 打印完成的回调
            if (typeof success == "function") success({
              state: 0,
              msg: "打印完成"
            })
            if (typeof complete == 'function') complete(c)
          } else {
            currentTime = 1
            this.print(config, currentTime, currentPrint);
          }
        }

      }
    })
  }


  /*
   * 打印标签
   */
  label() { }
}
