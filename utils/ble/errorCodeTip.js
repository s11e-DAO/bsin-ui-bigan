//错误码提示
export default function errorCodeTip(code) {
  if (code == 0) {
    //正常
  } else if (code == 10000) {
    uni.showToast({
      title: '未初始化蓝牙适配器',
      icon: 'none'
    })
  } else if (code == 10001) {
    uni.showToast({
      title: '当前蓝牙适配器不可用',
      icon: 'none'
    })
  } else if (code == 10002) {
    uni.showToast({
      title: '没有找到指定设备',
      icon: 'none'
    })
  } else if (code == 10003) {
    uni.showToast({
      title: '连接失败',
      icon: 'none'
    })
  } else if (code == 10004) {
    uni.showToast({
      title: '没有找到指定服务',
      icon: 'none'
    })
  } else if (code == 10005) {
    uni.showToast({
      title: '没有找到指定特征值',
      icon: 'none'
    })
  } else if (code == 10006) {
    uni.showToast({
      title: '当前连接已断开',
      icon: 'none'
    })
  } else if (code == 10007) {
    uni.showToast({
      title: '当前特征值不支持此操作',
      icon: 'none'
    })
  } else if (code == 10008) {
    uni.showToast({
      title: '其余所有系统上报的异常',
      icon: 'none'
    })
  } else if (code == 10009) {
    uni.showToast({
      title: 'Android 系统特有，系统版本低于 4.3 不支持 BLE',
      icon: 'none'
    })
  }
}
