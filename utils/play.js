import { GOLOBAL_TYPE } from "../config/consts";




// let main = plus.android.runtimeMainActivity();

// let SpeechUtility = plus.android.importClass('com.iflytek.cloud.SpeechUtility');

// SpeechUtility.createUtility(main, "appid=53feacdd");

// let SynthesizerPlayer = plus.android.importClass('com.iflytek.cloud.SpeechSynthesizer');

// let play = SynthesizerPlayer.createSynthesizer(main, null);
let play, SpeechUtility, SynthesizerPlayer, main, receiver
let platform = uni.getSystemInfoSync().platform

if (platform == 'android') {
  // receiver = plus.android.implements('com.iflytek.cloud.SynthesizerListener', {
  //   onEvent: function (eventType, arg1, arg2, obj) {
  //     console.log("onEvent");
  //   },
  //   onSpeakBegin: function () {
  //     console.log("开始阅读");
  //   },
  //   onSpeakPaused: function () {
  //     console.log(" 暂停播放 ");
  //   },
  //   onSpeakResumed: function () {
  //     console.log("继续播放");
  //   },
  //   // onBufferProgress: function (percent, beginPos, endPos, info) {
  //   //   console.log("合成进度" + percent);
  //   // },
  //   // onSpeakProgress: function (percent, beginPos, endPos) {
  //   //   console.log("播放进度" + percent);
  //   // },
  //   onCompleted: function (error) {
  //     console.log("播放完毕");
  //   }
  // });
  // main = plus.android.runtimeMainActivity();
  // SpeechUtility = plus.android.importClass('com.iflytek.cloud.SpeechUtility');
  // SpeechUtility.createUtility(main, "appid=5eec6eec");
  // SynthesizerPlayer = plus.android.importClass('com.iflytek.cloud.SpeechSynthesizer');
  // play = SynthesizerPlayer.createSynthesizer(main, null);
}


export default {
  play: (musicSrc) => {
    if (global[GOLOBAL_TYPE.IS_PLAY_SOUND]) return
    const innerAudioContext = uni.createInnerAudioContext();
    innerAudioContext.src = musicSrc;
    innerAudioContext.autoplay = true;
    innerAudioContext.play()
    innerAudioContext.onPlay(() => {
      console.log('开始播放');
      global[GOLOBAL_TYPE.IS_PLAY_SOUND] = true
    });
    innerAudioContext.onEnded(() => {
      global[GOLOBAL_TYPE.IS_PLAY_SOUND] = false
      console.log('播放结束');
    });
    innerAudioContext.onError((res) => {
      console.log('播放错误', res);
    });
  },
  start: (str) => {
    if (platform == 'android' && !global[GOLOBAL_TYPE.IS_PLAY_SOUND]) {
      global[GOLOBAL_TYPE.IS_PLAY_SOUND] = true
      play.startSpeaking(str, null)
      setTimeout(() => {
        global[GOLOBAL_TYPE.IS_PLAY_SOUND] = false
      }, 1000 * 5);
    }
  },
  speak: (str) => {

    // if (platform == 'android' && !global[GOLOBAL_TYPE.IS_PLAY_SOUND]) {
    //   global[GOLOBAL_TYPE.IS_PLAY_SOUND] = true
    //   play.startSpeaking(str, null)
    //   setTimeout(() => {
    //     global[GOLOBAL_TYPE.IS_PLAY_SOUND] = false
    //   }, 1000 * 5);
    // }
  },
}


