import {
  EXPIRE_TIME
} from '../config/index.js'

import {
  STORAGE_KEY
} from '../config/consts.js'

/**
 * 检查token登录是否过期，true未过期
 */
export const checkExpire = (cb) => {
  let now = new Date().getTime()
  let expire = uni.getStorageSync(STORAGE_KEY.EXPIRE_TIME)
  let token = uni.getStorageSync(STORAGE_KEY.TOKEN_KEY)
  return token && now - expire < 0
}

/**
 * 设置token以及过期时间 
 */
export const setToken = (token) => {
  let now = new Date().getTime()
  uni.setStorageSync(STORAGE_KEY.TOKEN_KEY, token);
  uni.setStorageSync(STORAGE_KEY.EXPIRE_TIME, now + EXPIRE_TIME);
  return now + EXPIRE_TIME
}

export const clearToken = () => {
  uni.setStorageSync(STORAGE_KEY.TOKEN_KEY, '');
  uni.setStorageSync(STORAGE_KEY.EXPIRE_TIME, '');
  // uni.clearStorageSync();
}

export const getToken = () => {
  let token = uni.getStorageSync(STORAGE_KEY.TOKEN_KEY)
  return token
}

/**
 * 设置当前显示的shopid
 * @param {number} id 当前显示的shopid
 */
export const setActiveShopId = (id) => {
  uni.setStorageSync(STORAGE_KEY.ACTIVE_SHOP_ID, id);
}

/**
 * 获取当前显示的shopid
 */
export const getActiveShopId = () => {
  let shopId = uni.getStorageSync(STORAGE_KEY.ACTIVE_SHOP_ID);
  return shopId
}