import {
  BASE_URL
} from '../config/index.js'
import {
  HEADER_ANTHORIZATION
} from '../config/consts.js'

import {
  getToken
} from './auth.js'

import Request from 'luch-request'

const service = new Request()
service.setConfig((config) => { /* 设置全局配置 */
  config.baseUrl = BASE_URL /* 根域名不同 */
  // config.header = {
  //   ...config.header,
  // }
  return config
})

/**
 * 自定义验证器，如果返回true 则进入响应拦截器的响应成功函数(resolve)，否则进入响应拦截器的响应错误函数(reject)
 * @param { Number } statusCode - 请求响应体statusCode（只读）
 * @return { Boolean } 如果为true,则 resolve, 否则 reject
 */
// 有默认，非必写
service.validateStatus = (statusCode) => {
  let statusCodes = [200, 201, 202, 203, 204, 205, 206]
  return statusCodes.indexOf(statusCode) > -1
}

service.interceptor.request((config, cancel) => { /* 请求之前拦截器 */
  // uni.showLoading();
  // let header = config.header
  config.header[HEADER_ANTHORIZATION] = 'bearer ' + getToken()
  // config.header = {
  //   ...header
  // }
  /*
  if (!token) { // 如果token不存在，调用cancel 会取消本次请求，但是该函数的catch() 仍会执行
    cancel('token 不存在') // 接收一个参数，会传给catch((err) => {}) err.errMsg === 'token 不存在'
  }
  */
  return config
})

// 必须使用异步函数，注意
service.interceptor.response((response) => { /* 请求之后拦截器 */
  if (process.env.NODE_ENV !== 'production') console.log(response.config.url, response, response.statusCode)
  // uni.hideLoading();
  let statusCode = response.statusCode

  if (response.data.code) {
    console.log(response.data.errMsg);
    const msgWhiteList = ['该店铺未开通外送服务']

    if (!msgWhiteList.includes(response.data.errMsg)) {
      uni.showToast({
        title: response.data.errMsg,
        icon: "none"
      })
    }
  }

  if (statusCode == 401 || response.data.code == 401) {
    uni.reLaunch({
      url: '/pages/login/login'
    });
  }

  if (statusCode == 404 || response.data.code == 404) {
    // uni.reLaunch({
    //   url: '/pages/login/login'
    // });
  }

  return response && response.data
}, (error) => {
  let errStatus = error.response && error.response.status || error
  uni.hideLoading();
  // 请求错误做点什么
  uni.showToast({
    icon: 'none',
    title: '网络请求异常',
    duration: 2000
  });
  if (errStatus == '401') {
    uni.reLaunch({
      url: '/pages/login/login'
    });
  }
  console.log(`网络请求异常`, error)
  return error
})

export default service
