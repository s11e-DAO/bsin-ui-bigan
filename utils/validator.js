export const maxPrice = 9999999
export const priceMessage = `价格不能超过${maxPrice}`
export const maxStock = 9999999
export const stockMessage = `库存不能超过${maxStock}`
/**
 * 校验价格
 * @param {*} rule 
 * @param {*} value 
 * @param {*} callback 
 */
export const checkPric = (rule, value, callback) => {
  if (value > maxPrice) {
    return callback(new Error(priceMessage));
  } else {
    callback();
  }
}

/**
 * 校验库存
 * @param {*} rule 
 * @param {*} value 
 * @param {*} callback 
 */
export const checkStock = (rule, value, callback) => {
  if (value > maxStock) {
    return callback(new Error(stockMessage));
  } else {
    callback();
  }
}