import { deepStr } from './index'
/**
 * 数组排序
 * @param {array} list 需要排序的数组
 * @param {number} index 需要移动的节点的索引
 * @param {number} targetIndex 需要移动到的位置
 */
export const moveListItem = (list, index, targetIndex) => {

  let newTargetIndex = targetIndex
  let targetEle = deepStr(list[targetIndex])
  let ele = list.splice(index, 1)[0] // 取出要移动的元素
  ele.sort = index < targetIndex ? targetEle.sort + 1 : targetEle.sort - 1
  list.splice(newTargetIndex, 0, ele) // 插入到交换的位置
  return ele
}
