export default {
  install: function (Vue, option) {
    //注册一个全局自定义指令 “v-float
    Vue.directive('floate', {
      inserted(el, binding) {
        const { value } = binding
        let { formData, key } = value
        // let vm = new Vue()
        el.addEventListener('change', v => {
          console.log(value);
          if (formData[key] || formData[key] === 0) {
            let value = Number(formData[key]);
            formData[key] = parseFloat(value.toFixed(2));
          }
        })
      }
    })
  }
}


