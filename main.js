import Vue from 'vue'
import App from './App'
import store from './store/index.js'
import './uni.promisify.adaptor'
import './plugins'
Vue.config.productionTip = false
App.mpType = 'app'

const app = new Vue({
  ...App,
  store
})
app.$mount()
