import { PUSH_PAYLOAD, EMIT_TYPE, GOLOBAL_TYPE } from '../config/consts'
import store from '../store'
import Vue from 'vue'
let _vm = new Vue()
let platform = uni.getSystemInfoSync().platform
const messageFun = {
  // 外送订单
  [PUSH_PAYLOAD.SOCKET_DELIVERY_ORDER]: (data) => {
    let params = {
      status: 2,
      showOrderType: 0
    };
    store.commit('SET_GLOBAL_DATA', { ORDER_PAGE_INIT_DATA: params })
    if (store.getters.global.ORDER_PAGE_HAS_SHOW) {
      uni.$emit(EMIT_TYPE.SET_ORDER_STATUS, params);
    }
    _vm.$goto("/pages/order/order", params, "tab");
  },
  // 店内订单
  [PUSH_PAYLOAD.SOCKET_ORDER]: (data) => {
    let params = {
      status: 1,
      showOrderType: 1
    };
    store.commit('SET_GLOBAL_DATA', { ORDER_PAGE_INIT_DATA: params })

    if (store.getters.global.ORDER_PAGE_HAS_SHOW) {
      uni.$emit(EMIT_TYPE.SET_ORDER_STATUS, params);
    }

    _vm.$goto("/pages/order/order", params, "tab");
  }
}

// #ifdef APP-PLUS
console.log('platform', platform);
plus.push.addEventListener("click", ({ payload }) => {
  try {
    console.log('payload', JSON.parse(payload));
    const { type, data } = JSON.parse(payload)
    messageFun[type](data)
  } catch (error) {

  }
}, false);
// #endif



