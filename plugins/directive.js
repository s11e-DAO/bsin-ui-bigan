import Vue from 'vue'

import floatFormat from '../components/public/directive/floatFormat'

Vue.use(floatFormat)