import Vue from 'vue'
import * as consts from '../config/consts.js'
import schema from "async-validator";
import { deepStr, isNull, checkLimitPrice } from '../utils/index'
let goto = (url, query, type) => {
  let queryStr = ''
  let arr = []
  for (let key in query) {
    arr.push(`${key}=${query[key]}`)
  }
  queryStr = arr.join('&')
  let fullPath = url.indexOf('/') > -1 ? url : `/pages/${url}/${url}`
  if (arr.length > 0) fullPath = fullPath + '?' + queryStr
  if (type == 'tab') {
    uni.switchTab({
      url: fullPath
    })
  } else if (type == 'redirect') {
    uni.redirectTo({
      url: fullPath
    });
  } else if (type == 'reLaunch') {
    uni.reLaunch({
      url: fullPath
    });
  } else {
    uni.navigateTo({
      url: fullPath
    })
  }

}

let back = () => {
  uni.navigateBack();
}
Vue.prototype.$goto = goto

Vue.prototype.$back = back

Vue.prototype.$consts = consts

Vue.prototype.$deepStr = deepStr

Vue.prototype.$validate = schema

Vue.prototype.$isNull = isNull

Vue.prototype.$checkLimitPrice = checkLimitPrice
