import Vue from 'vue'
import Form from '../components/public/Form/index.vue'
import FormItem from '../components/public/FormItem/index.vue'
import TButton from '../components/public/Button/TButton.vue'

Vue.component('MzForm', Form)
Vue.component('MzFormItem', FormItem)
Vue.component('TButton', TButton)
let toast = (title) => {
  uni.showToast({
    icon: "none",
    title: title
  })
}
let showLoading = (title) => {
  uni.showLoading({
    title: title
  });
}

let hideLoading = (title) => {
  uni.hideLoading({
    title: title,
    mask: true
  });
}
Vue.prototype.$toast = toast
Vue.prototype.$showLoading = showLoading
Vue.prototype.$hideLoading = hideLoading