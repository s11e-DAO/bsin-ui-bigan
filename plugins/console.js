const NODE_ENV = process.env.NODE_ENV

console.log = (function (oriLogFunc) {
  return function () {
    //判断配置文件是否开启日志调试
    if (NODE_ENV!='production') {
      try{
        oriLogFunc.call(console, ...arguments);
      }catch(e){
        console.error('console.log error', e);
      }
    }
  }
})(console.log);
