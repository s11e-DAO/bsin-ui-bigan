import Vue from 'vue'
import dayjs from 'dayjs'
import relativeTime from 'dayjs/plugin/relativeTime'
import 'dayjs/locale/zh-cn'
dayjs.locale('zh-cn')
dayjs.extend(relativeTime)
import { FILE_PATH } from '../config/index'
// import defaultImg from '../assets/images/暂无图片.png'
function trim(str) {
  return str.replace(/(^\s*)(\s*$)/g, '')
}

function formatDate(date, format = 'YYYY-MM-DD HH:mm:ss') {
  // let d = dayjs(date).format(format)
  // let s = dayjs('2019-03-02 02:02:02', format).fromNow('s')
  // console.log(s);

  return dayjs(date).format(format)
}

// 格式化价格，保留两位小数
function formatPrice(price) {
  return Number(price).toFixed(2)
}

function simpleDate(date, format = 'YYYY-MM-DD HH:mm:ss') {
  // let d = dayjs(date).format(format)
  // let s = dayjs(d, format).fromNow()
  // console.log(s);

  

  return dayjs(date).format(format)
}

function formatFilePath(fileName) {
  // return fileName ? FILE_PATH + fileName : defaultImg
  return FILE_PATH + fileName
}


const filters = {
  trim, formatDate, formatFilePath, formatPrice
}
Object.keys(filters).forEach(key => {
  Vue.filter(key, filters[key])
})