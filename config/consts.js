import globalType from './global'
// socket������
export const TASK_NAME = {
  ORDER: "ORDER"
}

export const HEADER_ANTHORIZATION = 'Authorization'
// storage�е�keyֵ
export const STORAGE_KEY = {
  TOKEN_KEY: 'JWT_TOKEN', // token
  ACTIVE_SHOP_ID: 'ACTIVE_SHOP_ID', // token
  BLUETOOTH_INFO: "BLUETOOTH_INFO", // �����豸��Ϣ
  EXPIRE_TIME: "EXPIRE_TIME", // ʲôʱ�����
  APP_SETTINGS: "APP_SETTINGS", // 打印机设置
}

// �����Ͽ�����
export const BREAK_TYPE = {
  PASSIVE: "PASSIVE", // ����Ͽ�
  ACTIVE: "ACTIVE", // �����Ͽ�
}

export const EDIT_TYPE = {
  NORMAL: "NORMAL", // ����
  SORT: "SORT", // ����
  ADD: "ADD", // �½�
  MODIFY: "MODIFY" // �޸�
}

// 页面通讯类型
export const EMIT_TYPE = {
  SET_SKU_LIST: 'SET_SKU_LIST', // 设置skulist
  SET_PROPS: 'SET_PROPS', // 设置props
  SET_ORDER_STATUS: 'SET_ORDER_STATUS', // 设置订单页查询条件
  UPDATE_ORDER_LIST: 'UPDATE_ORDER_LIST', // 更新订单列表页
}

// 全局变量key
export const GOLOBAL_TYPE = {
  ...globalType
}

// push message payload

export const PUSH_PAYLOAD = {
  SOCKET_ORDER: "SOCKET_ORDER", // 收到店内订单
  SOCKET_DELIVERY_ORDER: "SOCKET_DELIVERY_ORDER", // 收到外送订单
}