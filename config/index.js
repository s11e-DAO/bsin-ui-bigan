//基础请求地址
export const BASE_URL = 'http://127.0.0.1:8097/gateway'

//通用请求地址
export const COMMON_BASE_URL = ''

// 图片上传地址
export const UPLOAD_PATH = ''

// 图片地址
export const FILE_PATH = 'https://mz-fe.oss-cn-beijing.aliyuncs.com/mz-fe/'

// socket地址
export const SOCKET_URL = ''

export const HEADER_ANTHORIZATION = 'Authorization'

// 登录有效时长 ms 7天expire
export const EXPIRE_TIME = 1000 * 60 * 60 * 24 * 15

// 二维码地址信息 http://www.maiziit.cn
export const qrcodeBaseUrl = ''

