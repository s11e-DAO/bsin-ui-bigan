export default {
  ORDER_PAGE_INIT_DATA: "ORDER_PAGE_INIT_DATA", // 订单页初始化数据
  ORDER_PAGE_HAS_SHOW: "ORDER_PAGE_HAS_SHOW", // 订单页是否已经显示
  IS_PLAY_SOUND: "IS_PLAY_SOUND", // 是否正在播放
}