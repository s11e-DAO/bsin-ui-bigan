!(function() {
	var envConfig = {
		themeColor: [15, 20, 25],
		appid: 'wx2af87034a33f7c93',
		// 重定向地址
		wxRedirectUrl: 'http://192.168.198.105:8097/gateway',
		ipfsUrl: 'http://192.168.198.166:8097/ipfsUpload',
		fileUploadUrl: 'http://192.168.198.166:8097/fileUpload2Local', //服务器本地存储
		adminIpfsUploadUrl: 'http://127.0.0.1:8097/adminIpfsUpload', // 图片上传地址:ipfs和服务器本地存储
		ipfsGatewauUrl: 'https://ipfs.s11edao.com/ipfs/', // ipfs Gateway
		// 接口请求地址http://192.168.198.168:8097 http://192.168.198.131:8097
		// baseURL: 'http://127.0.0.1:8097/gateway',
		baseURL: 'http://127.0.0.1:8097/gateway',
		/**
		 * 平台商户ID--火源社区商户
		 */
		platformMerchantNo: "1714684039891509250",
		/** 
		 * 租户id
		 */
		tenantId: '1693135291382108160',
		//1691638102227030016
		//不需要请求加载的名单

		chainEnv: 'test',
		chainType: 'conflux',

		dontNeedLoading: [{
			serviceName: 'ArticleService',
			methodName: 'getHomeCarouselList'
		}, {
			serviceName: 'CustomerGradeIncentiveEnginService',
			methodName: 'execute'
		}],
		//不需要token的请求
		dontNeedToken: [{
				serviceName: 'CustomerService',
				methodName: 'registerOrLogin'
			},
			{
				serviceName: 'ArticleService',
				methodName: 'getHomePageList'
			}, {
				serviceName: 'ArticleService',
				methodName: 'getHomeCarouselList'
			}, {
				serviceName: 'ArticleService',
				methodName: "getDetail"
			}, {
				serviceName: "ArticleService",
				methodName: "getDetail"
			},
			{
				serviceName: "MerchantService",
				methodName: "getPageList"
			},
			{
				serviceName: "DictService",
				methodName: "getDictItemList"
			},
			{
				serviceName: "MerchantService",
				methodName: "getStoreList"
			}, {
				serviceName: "MerchantService",
				methodName: "getDetail"
			}, {
				serviceName: "ArticleCategoryService",
				methodName: "getCategory"
			}, {
				serviceName: "ArticleService",
				methodName: "getPageList"
			}, {
				serviceName: "ProposalService",
				methodName: "getPageList"
			}, {
				serviceName: "DigitalAssetsItemService",
				methodName: "getPopularEquityList"
			}, {
				serviceName: "OrderbookService",
				methodName: "getPageList"
			}, {
				serviceName: "GoodsClassService",
				methodName: "getTopList"
			}, {
				serviceName: "GoodsService",
				methodName: "getPageList"
			}, {
				serviceName: "GoodsService",
				methodName: "getDetail"
			}, {
				serviceName: "ActivityService",
				methodName: "getPageList"
			}, {
				serviceName: "TaskService",
				methodName: "getPageList"
			}
		]
	};
	module.exports = envConfig;
})();