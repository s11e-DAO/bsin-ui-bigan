export const orderStatus = [
  { status: 0, label: "待付款" },
  { status: 1, label: "已付款" }, // 待接单
  { status: 2, label: "待配送" }, // 已接单
  { status: 3, label: "已配送" },
  { status: 4, label: "已收货" },
  { status: 5, label: "已评价" },
  { status: -1, label: "已取消" }
]

// 活动类型 1-充值满赠 2-折扣活动 3-入会送礼s
export const promotionTypes = [
  { type: 1, label: "充值满赠" },
  { type: 2, label: "折扣活动" },
  { type: 3, label: "入会送礼" },
]