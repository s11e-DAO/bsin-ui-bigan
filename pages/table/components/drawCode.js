import { base64ToPath } from "image-tools";


export default class DrawCode {

  constructor(canvasId) {
    this.canvasId = canvasId
  }

  draw(groups) {
    const context = uni.createCanvasContext(this.canvasId)
    groups.forEach(group => {
      group.tables.forEach(table => {
        context.drawImage(table.tableCode, 0, 0, 150, 100)
        context.draw()
      })
    })
  }

  save() {
    uni.canvasToTempFilePath({
      // x: 100,
      // y: 200,
      // width: 50,
      // height: 50,
      // destWidth: 100,
      // destHeight: 100,
      canvasId: "table-code-canvas",
      success: function (res) {
        // 在H5平台下，tempFilePath 为 base64
        console.log(res.tempFilePath);
        uni.saveImageToPhotosAlbum({
          // 将图片保存在手机
          filePath: res.tempFilePath, //保存的位置
          success: res => {
            uni.showToast({
              title: `二维码保存成功`,
              icon: "none"
            });
          },
          fail: error => {
            uni.showToast({
              title: `二维码保存失败`,
              icon: "none"
            });
            console.log("fail", error);
          }
        });
      }
    });
  }



}